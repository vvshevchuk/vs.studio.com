<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="style.css" type="text/css">
    <link rel="stylesheet" href="icons.css" type="text/css">
    <title>Vitalii Shevchuk web studio</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Anton&display=swap" rel="stylesheet">
    <script src="https://www.gstatic.com/charts/loader.js"></script>
    <script src="chart.js"></script>
</head>
<body>
<!-- -------------------- scrollUp --------------------------- -->
<div id="scroll-hide" class="scroll-up scroll-up--active">
    <svg class="scroll-up__svg" viewBox="-2 -2 52 52">
        <path
            class="scroll-up__svg-path"
            d="
                    M24,0
                    a24,24 0 0,1 0,48
                    a24,24 0 0,1 0,-48
                "
        />
    </svg>
</div>
<script src="scripts.js"></script>
<!-- -------------------- scrollUp - end --------------------- -->

<section class="se1-main">

    <div class="social-icons">
        <div class="social-icons-f-y-i">
            <a class="a-social-icons" href="#f"><i class="gg-facebook"></i></a>
            <a class="a-social-icons" href="#y"><i class="gg-youtube"></i></a>
            <a class="a-social-icons" href="#in"><i class="gg-instagram"></i></a>
        </div>
        <div id="mail-icon-email">
            <a id="icon-mail" class="a-social-icons" href="#mail"><i class="gg-mail"></i></a>
            <a id="mail" class="a-social-icons" href="#mail">vitaliishevchuk123@gmail.com</a>
        </div>
    </div>
    <div class="se1-1">
        <div class="se1-block-1">
            <h1 class="se1-h1">I'm a web
                <br>developer</h1>
        </div>
    </div>
    <div class="se1-2">
        <div class="se1-block-2">
        <h2 class="se1-h2">Vitalii Shevchuk</h2>
        </div>
    </div>
</section>
<section class="se2-main">
<div class="black-block"> <span> My professional skills</span></div>
</section>
<section class="se2-main">
    <div class="se2-block">
        <!-- Identify where the chart should be drawn. -->
        <div id="donutchart" style="width: 600px; height: 600px;"></div>
    </div>
    <div class="se3">
        <div class="se3-block">
            <h5 class="se3-block-h5">Web design</h5>
            <p class="se3-block-p">
                Web design encompasses many different skills and disciplines
                in the production and maintenance of websites. The different areas
                of web design include web graphic design; user interface design; authoring,
                including standardised code and proprietary software; user experience design;
                and search engine optimization.
            </p>
            <div class="se3-block-p-show">
                <p class="se3-block-p">
                    Often many individuals will work in teams covering different aspects of the design process,
                    although some designers will cover them all. The term "web design" is normally used to describe
                    the design process relating to the front-end (client side) design of a website including writing markup.
                    Web design partially overlaps web engineering in the broader scope of web development.
                    Web designers are expected to have an awareness of usability and if their role involves creating
                    markup then they are also expected to be up to date with web accessibility guidelines.
                </p>
            </div>
            <button class="se3-block-button">Read more/less</button>
        </div>
    </div>
</section>
<div id="se3-main">
    <div class="se3-b">
        <p class="se1-left-text-p">
            There are two types of people who will tell you that you cannot make
            a difference in this world: those who are afraid to try and those who are afraid you will succeed.
        </p>
        <div class="se3-show">
            <p class="se1-left-text-p">
                When people try to do something different, unique and bold, they will hear a range of voices:
                some in cautioning tones, some in flat-out opposition, and some in valued support.

                It’s important to remember that the voices which are holding you back are rooted in an insecurity,
                in either a lack of imagination, a fear of stepping out of bounds, or, as Ray Goforth says,
                a fear that you will succeed, because if you succeed, then they have no excuse to go out and accomplish their dreams.
            </p>
        </div>
        <button class="se3-b-button">Read more/less</button>
    </div>
    <div class="se3-b">
        <img class="se3-img-my" src="images/backgroundWithMe.png">
    </div>

</div>
<section class="se4-main-1"></section>
<section class="se4-main">
    <div class="se4">
        <h5 id="se4-h1" class="se3-block-h5">My work</h5>
    </div>
    <div class="se4">
        <img class="se4-img" src="images/1.png" onclick="openModal();currentSlide(1)"/>
        <img class="se4-img" src="images/8.jpg" onclick="openModal();currentSlide(2)"/>
        <img class="se4-img" src="images/3.png" onclick="openModal();currentSlide(3)"/>
    </div>
    <div class="se4">
        <img class="se4-img" src="images/4.png" onclick="openModal();currentSlide(4)"/>
        <img class="se4-img" src="images/5.png" onclick="openModal();currentSlide(5)"/>
        <img class="se4-img" src="images/6.png" onclick="openModal();currentSlide(6)"/>
    </div>
    <div class="se4">
        <img class="se4-img" src="images/7.png" onclick="openModal();currentSlide(7)"/>
        <img class="se4-img" src="images/2.png" onclick="openModal();currentSlide(8)"/>
        <img class="se4-img" src="images/9.jpg" onclick="openModal();currentSlide(9)"/>
    </div>
    <!-------------------- Modal window ------------------------->
    <div id="myModal" class="modal">
        <span class="close cursor" onclick="closeModal()">&times;</span>
        <div class="modal-content">
        <div class="modal-main-img">
            <div class="mySlides">
                <div class="numbertext">1 / 9</div>
                <img src="images/1.png" style="width:100%">
            </div>

            <div class="mySlides">
                <div class="numbertext">2 / 9</div>
                <img src="images/8.jpg" style="width:100%">
            </div>

            <div class="mySlides">
                <div class="numbertext">3 / 9</div>
                <img src="images/3.png" style="width:100%">
            </div>

            <div class="mySlides">
                <div class="numbertext">4 / 9</div>
                <img src="images/4.png" style="width:100%">
            </div>

            <div class="mySlides">
                <div class="numbertext">5 / 9</div>
                <img src="images/5.png" style="width:100%">
            </div>

            <div class="mySlides">
                <div class="numbertext">6 / 9</div>
                <img src="images/6.png" style="width:100%">
            </div>

            <div class="mySlides">
                <div class="numbertext">7 / 9</div>
                <img src="images/7.png" style="width:100%">
            </div>

            <div class="mySlides">
                <div class="numbertext">8 / 9</div>
                <img src="images/8.jpg" style="width:100%">
            </div>

            <div class="mySlides">
                <div class="numbertext">9 / 9</div>
                <img src="images/9.jpg" style="width:100%">
            </div>
        </div>
            <a class="prev" onclick="plusSlides(-1)">&#10094;</a>
            <a class="next" onclick="plusSlides(1)">&#10095;</a>

            <div class="caption-container">
                <p id="caption"></p>
            </div>

        <div class="bottom-images-modal">
            <div class="column">
                <img class="demo cursor" src="images/1.png" style="width:100%" onclick="currentSlide(1)" alt="">
            </div>
            <div class="column">
                <img class="demo cursor" src="images/2.png" style="width:100%" onclick="currentSlide(2)" alt="">
            </div>
            <div class="column">
                <img class="demo cursor" src="images/3.png" style="width:100%" onclick="currentSlide(3)" alt="">
            </div>
            <div class="column">
                <img class="demo cursor" src="images/4.png" style="width:100%" onclick="currentSlide(4)" alt="">
            </div>
            <div class="column">
                <img class="demo cursor" src="images/5.png" style="width:100%" onclick="currentSlide(5)" alt="">
            </div>
            <div class="column">
                <img class="demo cursor" src="images/6.png" style="width:100%" onclick="currentSlide(6)" alt="">
            </div>
            <div class="column">
                <img class="demo cursor" src="images/7.png" style="width:100%" onclick="currentSlide(7)" alt="">
            </div>
            <div class="column">
                <img class="demo cursor" src="images/8.jpg" style="width:100%" onclick="currentSlide(8)" alt="">
            </div>
            <div class="column">
                <img class="demo cursor" src="images/9.jpg" style="width:100%" onclick="currentSlide(9)" alt="">
            </div>
        </div>
        </div>
    </div>
    <script src="slider.js"></script>
    <!-------------------- Modal window - end ------------------------>
</section>
<section class="se5-main"></section>
</body>
</html>
