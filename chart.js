google.charts.load("current", {packages:["corechart"]});
google.charts.setOnLoadCallback(drawChart);
function drawChart() {
    var data = google.visualization.arrayToDataTable([
        ['Skills', 'Level'],
        ['PHP',     20],
        ['MySQL',    10],
        ['HTML',      14],
        ['CSS',  12],
        ['JavaScript', 10],
        ['jQuery',    7],
        ['Ajax',    8],
        ['Bootstrap',    10],
        ['Git',    5],
        ['Laravel', 10],
    ]);

    var options = {
        pieHole: 0.4,
        slices: {  0: {offset: 0.2,
                    color: '#525252'},
                },
        pieSliceText: 'label',
        legend: 'none',
        backgroundColor: '#171717',
        chartArea:{
            padding:5,
            stroke:'red',
            },
        colors: ['#78502f'],

        fontSize:18,
        fontName: 'Calibri',
        pieStartAngle: 1,
        pieResidueSliceColor: '#34eb5e',
        tooltip: {
            text: 'percentage',
            showColorCode: true,
        },

    };

    var tooltip = {
        text: 'percentage',
    };


    var chart = new google.visualization.PieChart(document.getElementById('donutchart'));
    chart.draw(data, options);

}